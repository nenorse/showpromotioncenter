﻿using SmartStore.Core.Domain.Security;
using SmartStore.Services.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SmartStoreHelper
{
    public partial class Main : Form
    {

        public class AccountPassword
        {
            public int CompanyId;
            public string Password;
        }
        private List<AccountPassword> passwords;

        public Main()
        {
            InitializeComponent();
        }

        private void btnPasswords_Click(object sender, EventArgs e)
        {
            ReadPasswords();

            SetPasswords();

        }

        void SetPasswords()
        {

            var _securitySettings = new SecuritySettings()
            {
                EncryptionKey = "273ece6f97dd844d"
            };
            var _encryptionService = new EncryptionService(_securitySettings);

            using (SqlConnection cn = new SqlConnection(txtConnection.Text))
            {
                cn.Open();
                foreach (var accountPassword in passwords)
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {

                        string saltKey = _encryptionService.CreateSaltKey(5);
                        string password = _encryptionService.CreatePasswordHash(accountPassword.Password, saltKey, "SHA1");
                        cmd.Connection = cn;
                        cmd.CommandText = "UPDATE Customer set Password = '" + password + "', PasswordSalt='" + saltKey + "' Where AdminComment = '" + accountPassword.CompanyId.ToString() + "'";
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                    }
                }
                Debug.WriteLine(String.Format("Read {0} passwords", passwords.Count()));
            }

        }

        void ReadPasswords()
        {
            passwords = new List<AccountPassword>();
            using (SqlConnection cn = new SqlConnection(txtConnection.Text))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = cn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT [Company Id], [Password] FROM FIE19Accounts";
                    var reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        passwords.Add(new AccountPassword
                        {
                            CompanyId = int.Parse(reader["Company Id"].ToString()),
                            Password = reader["Password"].ToString()
                        });
                    }
                }
                Debug.WriteLine(String.Format("Read {0} passwords", passwords.Count()));
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
