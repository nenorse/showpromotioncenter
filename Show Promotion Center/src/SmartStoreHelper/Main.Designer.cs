﻿namespace SmartStoreHelper
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.txtConnection = new System.Windows.Forms.TextBox();
            this.lblConnectionString = new System.Windows.Forms.Label();
            this.btnPasswords = new System.Windows.Forms.Button();
            this.txtDev = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtConnection
            // 
            this.txtConnection.Location = new System.Drawing.Point(122, 25);
            this.txtConnection.Multiline = true;
            this.txtConnection.Name = "txtConnection";
            this.txtConnection.Size = new System.Drawing.Size(639, 54);
            this.txtConnection.TabIndex = 0;
            this.txtConnection.Text = resources.GetString("txtConnection.Text");
            // 
            // lblConnectionString
            // 
            this.lblConnectionString.AutoSize = true;
            this.lblConnectionString.Location = new System.Drawing.Point(25, 28);
            this.lblConnectionString.Name = "lblConnectionString";
            this.lblConnectionString.Size = new System.Drawing.Size(91, 13);
            this.lblConnectionString.TabIndex = 1;
            this.lblConnectionString.Text = "Connection String";
            // 
            // btnPasswords
            // 
            this.btnPasswords.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnPasswords.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPasswords.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPasswords.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnPasswords.Location = new System.Drawing.Point(281, 105);
            this.btnPasswords.Name = "btnPasswords";
            this.btnPasswords.Size = new System.Drawing.Size(244, 48);
            this.btnPasswords.TabIndex = 2;
            this.btnPasswords.Text = "Set Passwords";
            this.btnPasswords.UseVisualStyleBackColor = false;
            this.btnPasswords.Click += new System.EventHandler(this.btnPasswords_Click);
            // 
            // txtDev
            // 
            this.txtDev.Enabled = false;
            this.txtDev.Location = new System.Drawing.Point(122, 365);
            this.txtDev.Multiline = true;
            this.txtDev.Name = "txtDev";
            this.txtDev.Size = new System.Drawing.Size(639, 54);
            this.txtDev.TabIndex = 3;
            this.txtDev.Text = resources.GetString("txtDev.Text");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(119, 338);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "DEV String";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(786, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtDev);
            this.Controls.Add(this.btnPasswords);
            this.Controls.Add(this.lblConnectionString);
            this.Controls.Add(this.txtConnection);
            this.Name = "Main";
            this.Text = "SmartStore.Net HELPER";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtConnection;
        private System.Windows.Forms.Label lblConnectionString;
        private System.Windows.Forms.Button btnPasswords;
        private System.Windows.Forms.TextBox txtDev;
        private System.Windows.Forms.Label label1;
    }
}

